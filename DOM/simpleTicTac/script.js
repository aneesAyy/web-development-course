var restart = document.querySelector("#reset");
var el = document.querySelectorAll('.grid-item');

function clearBoard() {
    for (let index = 0; index < el.length; index++) {
        el[i].textContent = '';
    }
}

restart.addEventListener('click', clearBoard)

function changeMarker() {
    if (this.textContent === '') {
        this.textContent = 'X';
    } else if (this.textContent === 'X') {
        this.textContent = 'O';
    } else {
        this.textContent = '';
    }
}

for (var i = 0; i < el.length; i++) {
    el[i].addEventListener('click', changeMarker)
}