var headOne = document.querySelector("#one")
var headTwo = document.querySelector("#two")
var headThree = document.querySelector("#three")

headOne.addEventListener('mouseover', function() {
    headOne.textContent = "Mouse over here ahh";
    headOne.style.color = "red";
})

headOne.addEventListener('mouseout', function() {
    headOne.textContent = "Hover here";
    headOne.style.color = "black";

})

headTwo.addEventListener('click', function() {
    headTwo.textContent = "Got Clicked";
    headTwo.style.color = "gray";
})

headThree.addEventListener('dblclick', function() {
    headThree.textContent = "Got double Clicked";
    headThree.style.color = "cyan";
})