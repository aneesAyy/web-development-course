var header = document.querySelector(".colorHeading");

function getRandomColor() {
    var data = "0123456789ABCDEF";
    var color = "#";

    for (let index = 0; index < 6; index++) {
        color += data[Math.floor(Math.random() * 16)];
    }
    return color;
}

console.log(getRandomColor());

function changeColor(elem) {
    colorInput = getRandomColor();
    elem.style.color = colorInput;
}

setInterval("changeColor(header)", 500);

//! Accessing elements and chaging attributes

var specialA = document.querySelector("#special");
var specialB = specialA.querySelector("a");

var att = specialB.getAttribute("href");

specialB.setAttribute("href", "http://www.google.com");