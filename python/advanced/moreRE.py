import re

def multi_re_find(patterns, phrase):
    for pat in patterns:
        print(f"\nSearching for patterns {pat}")
        return (re.findall(pat, phrase))

test_phrase = "xdxxdxxx..d..s..x.d.xd...xd..axdxd.x.xddxdxxxdddddx"

# test_pattern = ['xd+'] # for 1 or more d's
# test_pattern = ['xd*'] # for zero or more d's 
test_pattern = ['xd{1,2}'] # for custom values 
test_pattern = ['xd[xd]+']

print(multi_re_find(test_pattern, test_phrase))


#! to seperate a string at punctuations or to remove punctuations

test_phrase = 'This is a string! But? has some punct...uations. Now, to remove them all'

test_pattern = ['[^!.?,]+']

result = ''.join(multi_re_find(test_pattern, test_phrase))
print(result)

#! Lowercase characters 

test_phrase = 'This is a string! But? has some punct...uations. Now, to remove them all'

test_pattern = ['[a-z]+']

result = ''.join(multi_re_find(test_pattern, test_phrase))
print(result)

#! Digits or non-digits

test_phrase = 'This string! has some% numbers 124 and 1 symbol @jeor00'

test_pattern = [r'\d'] # All the digits
# test_pattern = [r'\D'] # Everything except the digits

result = ''.join(multi_re_find(test_pattern, test_phrase))
print(result)

