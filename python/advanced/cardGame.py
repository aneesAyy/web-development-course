from random import shuffle

# To create the cards   
SUITE  = 'Hearts Diamonds Spades Clubs'.split()
RANKS = '2 3 4 5 6 7 8 9 10 Jack King Queen Ace'.split()

class Hand():
    def __init__(self, cards):
        self.cards = cards

    def __str__(self):
        return f"{self.cards}"

class Deck():
    def __init__(self):
        self.mycards = [(s,r) for s in SUITE for r in RANKS]

    def shuffle(self):
        shuffle(self.mycards)

    def split_in_half(self):
        return (self.mycards[:26], self.my[26:])


class Player():
    def __init__(self):
        pass

cards = Deck()

for s,r in cards.mycards:
    print(r + " of " + s)
# print(card.show())
