def func():
    print("Function in one.py")

print("one.py top level")

if __name__ == "__main__":
    print("one.py is being run directly")
else:
    print("one.py has been imported")