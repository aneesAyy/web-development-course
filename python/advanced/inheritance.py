class Animal():
    def __init__(self):
        print("ANIMAL CREATED")
    
    def whoAmI(self):
        print("ANIMAL")     
    def eat(self):
        print("EATING")

class Dog(Animal):  
    def __init__(self):
        # Animal.__init__(self)
        print("DOG CREATED")

    def bark(self):
        print("Wolf")

    def eat(self):
        print("Dog eating")

# mya = Animal()
# mya.whoAmI()
# mya.eat()

mydog = Dog()
mydog.eat()