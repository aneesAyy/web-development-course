class Dog():
    #CLASS OBJECT ATTRIBUTE
    species = "mammal"

    def __init__(self, breed, name):
        self.breed = breed
        self.name = name

# mydog = Dog(breed = "Local", name="Yesn't")
mydog = Dog("husky", "ooga")

# print(mydog.name + " : " + mydog.breed)
# print(mydog.species)

class Circle():
    pi = 3.14

    def __init__(self, radius = 1):
        self.radius = radius

    def area(self):
        return self.radius*self.radius * Circle.pi

    def set_radius(self, radius):
        self.radius = radius

myc = Circle(3)
print(myc.area())
print(myc.radius)

myc.radius = 1000
# myc.set_radius(10) # same thing 
print(myc.radius)