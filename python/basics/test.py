def add_num(num1, num2):
    if type(num1) == type(num2) == int:
        return num1 + num2
    else:
        return 'Sorry, integers needed!'


result = add_num(2, 23)
print(result)

# ! Lambda expressions

mylist = [1, 2, 4, 5, 6, 7, 8, 23, 29, 32]
evens = filter(lambda num: num % 2 == 0, mylist)
print(list(evens))

# ! THe split() method

tweet = "This is amazing #sports #allwelldone"
res = tweet.split('#')  
print(res)

# ! in keyword
num_list = list(x for x in range(1,10))
print(num_list)
