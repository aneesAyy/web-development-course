def array_check(nums):
    for n in nums:
        if nums[n] == 1 and nums[n+1] == 2 and nums[n+2] == 3:
            return True
    else:
        return False


mylist1 = [1, 3, 1, 2, 3, 0]
mylist2 = [1, 3, 2, 1, 2, 5, 6]

# print(array_check(mylist1))
# print(array_check(mylist2))


def end_other(l1, l2):
    if l1.lower() in l2.lower() or l2.lower() in l1.lower():
        return True 
    else:
        return False


print(end_other('hey', 'nohey'))
print(end_other('karroblah', 'blah'))
print(end_other('spotlight', 'moonlight'))
