from random import randint

def check_answer(passcode, guess):
    """ Compare the guess to the generated passcode 
        Show which numbers were guessed correctly
    """

    compare = ['X', 'X', 'X']
    num_correct = 0

    for i in range(0, 3):
        if passcode[i] == guess[i]:
            compare[i] = passcode[i]
            num_correct += 1
        else:
            pass

    print(f"You got {num_correct} numbers correct: {''.join(compare)}")

    if num_correct == 3:
        print("You win!")
        exit(1)
    else:
        pass


def keypad():
    """ Generate a random 3-digit passcode
        Provide 10 changes for the user to guess it
    """

    passcode = '%s%s%s' % (randint(1, 9), randint(1, 9), randint(1, 9))
    num_guesses = 10

    while num_guesses > 0:
        guess = input(">")

        if len(guess) != 3:
            print("That is not a 3 digit number, you just wasted a guess")
            num_guesses -= 1
            print(f"You have {num_guesses} guesses left")
        else:
            check_answer(passcode, guess)
            num_guesses -= 1
            print(f"You have {num_guesses} guesses left")
            pass

    print("You have used up all your guesses")
    print("The number was %s" % passcode)
    exit(1)


print("I have randomly generated a three digit number. Try to guess it:")
keypad()
