var names = [];

var input = prompt("Input an operation: add, remove, display, quit");
input = input.toLowerCase();

while (input !== "quit") {
    if (input == "add") {
        var op = prompt("Input a name to add");
        names.push(op);

    } else if (input == "remove") {
        var op = prompt("Input a name to remove");
        var index = names.indexOf(op);
        names.splice(index, 1);

    } else if (input == "display") {
        console.log(names);

    } else if (input == "quit") {
        alert("Thank you for using this app!")
    }

    input = prompt("Input an operation");

}