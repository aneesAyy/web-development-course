from django.db import models

class Project(models.Model):
    titlte = models.CharField(max_length = 100)
    description = models.TextField(max_length=250)
    image = models.ImageField(upload_to='portfolio/images')
    url = models.URLField(blank=True)